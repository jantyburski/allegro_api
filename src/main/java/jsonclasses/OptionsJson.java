package jsonclasses;

public class OptionsJson {
    private Boolean variantsByColorPatternAllowed;
    private Boolean advertisement;
    private Boolean advertisementPriceOptional;
    private Boolean offersWithProductPublicationEnabled;
    private Boolean productCreationEnabled;
    private Boolean productEANRequired;

    public Boolean getVariantsByColorPatternAllowed() {
        return variantsByColorPatternAllowed;
    }

    public void setVariantsByColorPatternAllowed(Boolean variantsByColorPatternAllowed) {
        this.variantsByColorPatternAllowed = variantsByColorPatternAllowed;
    }

    public Boolean getAdvertisementPriceOptional() {
        return advertisementPriceOptional;
    }

    public void setAdvertisementPriceOptional(Boolean advertisementPriceOptional) {
        this.advertisementPriceOptional = advertisementPriceOptional;
    }

    public Boolean getOffersWithProductPublicationEnabled() {
        return offersWithProductPublicationEnabled;
    }

    public void setOffersWithProductPublicationEnabled(Boolean offersWithProductPublicationEnabled) {
        this.offersWithProductPublicationEnabled = offersWithProductPublicationEnabled;
    }

    public Boolean getProductCreationEnabled() {
        return productCreationEnabled;
    }

    public void setProductCreationEnabled(Boolean productCreationEnabled) {
        this.productCreationEnabled = productCreationEnabled;
    }

    public Boolean getProductEANRequired() {
        return productEANRequired;
    }

    public void setProductEANRequired(Boolean productEANRequired) {
        this.productEANRequired = productEANRequired;
    }

    public Boolean getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(Boolean advertisement) {
        this.advertisement = advertisement;
    }
}
