package jsonclasses;

public class CategoryJson {
    private String id;
    private String name;
    private Boolean parent;
    private Boolean leaf;
    private OptionsJson optionsJson = new OptionsJson();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getParent() {
        return parent;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public OptionsJson getOptionsJson() {
        return optionsJson;
    }

    public void setOptionsJson(OptionsJson optionsJson) {
        this.optionsJson = optionsJson;
    }
}
