package jsonclasses;

public class AutorizationJson {
    private String access_token;
    private String token_type;
    private int expires_in;
    private String scope;
    private String allegro_api;
    private String jti;

    public AutorizationJson() {
    }


    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getAllegro_api() {
        return allegro_api;
    }

    public void setAllegro_api(String allegro_api) {
        this.allegro_api = allegro_api;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }
}
