package lib;

import com.google.gson.Gson;
import jsonclasses.CategoryJson;

import java.io.IOException;
import java.net.http.HttpResponse;

public class Parameters implements IJsonDecode{
    private HttpConnection httpConnection;
    private HttpResponse<String> response;

    public Parameters(HttpConnection httpConnection) {
        this.httpConnection = httpConnection;
    }

    public String getParametersSupportedByACategory(String url, String headerName1, String headerValue1, String headerName2, String headerValue2, String headerName3, String headerValue3, String json) {
        HttpRequests httpRequest = new HttpRequests();

        try {
            response = httpConnection.getClient().send(httpRequest.getMethod(url,headerName1,headerValue1,headerName2,headerValue2,headerName3,headerValue3), HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(response.body());
        return null;
    }

    public String getRespone() {
        return response.body();
    }


    @Override
    public String jsonStringDecode(String json) {
        Gson gson = new Gson();
        CategoryJson category = gson.fromJson(json, CategoryJson.class);
        return category.getName();
    }
}
