package lib;
import java.net.*;
import java.net.http.HttpClient;
import java.time.Duration;

public class HttpConnection {
    private HttpClient client;

    HttpConnection() {
        client =  HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();
    }

    public HttpClient getClient() {
        return client;
    }
}

