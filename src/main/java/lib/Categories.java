package lib;

import com.google.gson.Gson;
import jsonclasses.CategoriesJson;

import java.io.IOException;
import java.net.http.HttpResponse;

public class Categories {
    private HttpConnection httpConnection;
    private HttpResponse<String> response;

    public Categories(HttpConnection httpConnection) {
        this.httpConnection = httpConnection;
    }
    public String getIDsOfAllegroCategories(String url, String headerName1, String headerValue1, String headerName2, String headerValue2, String headerName3, String headerValue3, String json) {
        HttpRequests httpRequest = new HttpRequests();

        try {
            response = httpConnection.getClient().send(httpRequest.getMethod(url,headerName1,headerValue1,headerName2,headerValue2,headerName3,headerValue3), HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(getRespone());
        return null;
    }

    public String getRespone() {
        return response.body();
    }
}
