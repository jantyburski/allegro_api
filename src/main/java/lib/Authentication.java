package lib;

import com.google.gson.Gson;
import jsonclasses.AutorizationJson;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.Base64;

public class Authentication implements IJsonDecode{
    private final String appName = "api_acceptance_tests";
    private final String type = "DEVICE";
    private final String clientID = "91602d05dbf648e29171c63e3966f5fa";
    private final String clientSecret = "b1twiGlskYv8vVXdmmCBgSouiiPyIjnFOdA00tXQWrpN8YQmEjV9zShriKE3ZO3s";
    private final String initUrl = "https://allegro.pl/auth/oauth/token?grant_type=client_credentials";
    private final String headerName = "Authorization";
    private final String headerValue = "Basic "+ Base64.getEncoder().encodeToString((clientID+":"+clientSecret).getBytes());
    private String access_token;
    private String token_type;
    private int expires_in;
    private HttpConnection httpConnection;
    private HttpResponse<String> response;


    public Authentication() {
        access_token = "";
        token_type = "";
        expires_in = 0;
        httpConnection = new HttpConnection();
    }

    public String init(String json) {
        HttpRequests httpRequest = new HttpRequests();

        try {
            response = httpConnection.getClient().send(httpRequest.postMethod(initUrl,headerName,headerValue,json),HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(response.body());
        return response.body();
    }



    public String getAppName() {
        return appName;
    }

    public String getType() {
        return type;
    }

    public String getClientID() {
        return clientID;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public String getResponse() {
        return response.body();
    }

    public HttpConnection getHttpConnection() {
        return httpConnection;
    }

    @Override
    public String jsonStringDecode(String json) {
        Gson gson = new Gson();
        AutorizationJson autorizationJson = gson.fromJson(json, AutorizationJson.class);
        this.access_token = autorizationJson.getAccess_token();
        this.token_type = autorizationJson.getToken_type();
        this.expires_in = autorizationJson.getExpires_in();
        return access_token;
    }
}
