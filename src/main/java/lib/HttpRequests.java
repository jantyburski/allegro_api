package lib;

import java.net.URI;
import java.net.http.HttpRequest;

public class HttpRequests {
    private HttpConnection httpConnection;

    HttpRequests() {
        httpConnection = new HttpConnection();
    }

    public HttpRequest getMethod(String url, String name, String value)
    {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader(name, value)
                .GET()
                .build();
        return request;
    }

    public HttpRequest getMethod(String url, String name1, String value1, String name2, String value2)
    {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader(name1, value1)
                .setHeader(name2, value2)
                .GET()
                .build();
        return request;
    }

    public HttpRequest getMethod(String url, String name1, String value1, String name2, String value2, String name3, String value3)
    {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader(name1, value1)
                .setHeader(name2, value2)
                .setHeader(name3, value3)
                .GET()
                .build();
        return request;
    }

    public HttpRequest postMethod(String url, String name, String value, String json)
    {
        HttpRequest mainRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader(name, value)
                .POST(java.net.http.HttpRequest.BodyPublishers.ofString(json))
                .build();
        return mainRequest;
    }

    public HttpRequest postMethod(String url, String name1, String value1, String name2, String value2, String json)
    {
        HttpRequest mainRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader(name1, value1)
                .setHeader(name2, value2)
                .POST(java.net.http.HttpRequest.BodyPublishers.ofString(json))
                .build();
        return mainRequest;
    }

    public HttpRequest postMethod(String url, String name1, String value1, String name2, String value2, String name3, String value3, String json)
    {
        HttpRequest mainRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader(name1, value1)
                .setHeader(name2, value2)
                .setHeader(name3, value3)
                .POST(java.net.http.HttpRequest.BodyPublishers.ofString(json))
                .build();
        return mainRequest;
    }
}
