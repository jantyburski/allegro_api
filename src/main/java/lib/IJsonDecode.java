package lib;

public interface IJsonDecode {
    public String jsonStringDecode(String json);
}
