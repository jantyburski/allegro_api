import lib.Authentication;
import lib.Categories;
import lib.Category;
import lib.Parameters;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.regex.Pattern;

public class TestApi {
    private String[] ids;
    private int countIDs = 0;

    @Test
    public void getIDsOfAllegroCategories() throws InterruptedException {
        System.out.println("getIDsOfAllegroCategories");
        getCategories();
    }

    @Test
    public void getACategoryByID() throws InterruptedException {
        System.out.println("getACategoryByID");
        Authentication authentication = getCategories();
        Category category = new Category(authentication.getHttpConnection());
        Random random = new Random();
        String idsValue = ids.length > 0 ? ids[random.nextInt(ids.length)] : "5";
        String url = "https://api.allegro.pl/sale/categories/"+idsValue;
        String headerName1 = "Accept";
        String value1 = "application/vnd.allegro.public.v1+json";
        String headerName2 = "Authorization";
        String value2 = "Bearer " + authentication.getAccess_token();
        String headerName3 = "Accept-Language";
        String value3 = "pl-PL";
        Thread.sleep(6000);
        category.getACategoryByID(url, headerName1, value1, headerName2, value2, headerName3, value3,"");
        Assertions.assertTrue(category.getRespone().contains("name"));

    }

    @Test
    public void getParametersSupportedByACategory() throws InterruptedException {
        System.out.println("getParametersSupportedByACategory");
        Authentication authentication = getCategories();
        Parameters parameters = new Parameters(authentication.getHttpConnection());
        Random random = new Random();
        String idsValue = ids.length > 0 ? ids[random.nextInt(ids.length)] : "5";
        String url = "https://api.allegro.pl/sale/categories/"+idsValue+"/parameters";
        String headerName1 = "Accept";
        String value1 = "application/vnd.allegro.public.v1+json";
        String headerName2 = "Authorization";
        String value2 = "Bearer " + authentication.getAccess_token();
        String headerName3 = "Accept-Language";
        String value3 = "pl-PL";
        Thread.sleep(6000);
        parameters.getParametersSupportedByACategory(url, headerName1, value1, headerName2, value2, headerName3, value3,"");
        Assertions.assertTrue(parameters.getRespone().contains("options"));
    }
    String[] getIds(String json) {
        String[] test = json.split("\"id\":\"");
        ids = new String[test.length];
        for (int i = 1; i < test.length; ++i)
        {
            ids[i] = test[i].split("\"")[0];
        }
        return ids;
    }

    public Authentication getCategories() throws InterruptedException {
        Authentication authentication = new Authentication();
        authentication.init("");
        authentication.jsonStringDecode(authentication.getResponse());
        Thread.sleep(6000);
        Categories categories = new Categories(authentication.getHttpConnection());
        String url = "https://api.allegro.pl/sale/categories";
        String headerName1 = "Accept";
        String value1 = "application/vnd.allegro.public.v1+json";
        String headerName2 = "Authorization";
        String value2 = "Bearer " + authentication.getAccess_token();
        String headerName3 = "Accept-Language";
        String value3 = "pl-PL";
        categories.getIDsOfAllegroCategories(url, headerName1, value1, headerName2, value2, headerName3, value3,"");
        countIDs = getIds(categories.getRespone()).length;
        Assertions.assertTrue(getIds(categories.getRespone()).length>1);
        return authentication;
    }
}
